package com.parsdroid.nightdownloader.ui.main

import android.Manifest
import android.annotation.TargetApi
import android.app.AlertDialog
import android.app.FragmentTransaction
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.PorterDuff
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver
import android.widget.*
import com.parsdroid.nightdownloader.R
import com.parsdroid.nightdownloader.get.DownloadManager
import com.parsdroid.nightdownloader.service.DownloadManagerService
import com.parsdroid.nightdownloader.ui.adapter.NavigationAdapter
import com.parsdroid.nightdownloader.ui.common.FloatingActionButton
import com.parsdroid.nightdownloader.ui.common.ToolbarActivity
import com.parsdroid.nightdownloader.ui.fragment.AllMissionsFragment
import com.parsdroid.nightdownloader.ui.fragment.DownloadedMissionsFragment
import com.parsdroid.nightdownloader.ui.fragment.DownloadingMissionsFragment
import com.parsdroid.nightdownloader.ui.fragment.MissionsFragment
import com.parsdroid.nightdownloader.ui.settings.SettingsActivity
import com.parsdroid.nightdownloader.ui.web.BrowserActivity
import com.parsdroid.nightdownloader.util.CrashHandler
import com.parsdroid.nightdownloader.util.Utility
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import java.io.File
import java.io.IOException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class MainActivity : ToolbarActivity(), AdapterView.OnItemClickListener {

    private val disposable = CompositeDisposable()
    private lateinit var permissionObserver: Consumer<Boolean>

    private var mFragment: MissionsFragment? = null
    private var mDrawer: DrawerLayout? = null
    private var mList: ListView? = null
    private var mAdapter: NavigationAdapter? = null
    private var mToggle: ActionBarDrawerToggle? = null
    private var mManager: DownloadManager? = null
    private var mBinder: DownloadManagerService.DMBinder? = null

    private var mPendingUrl: String? = null
    private var mPrefs: SharedPreferences? = null
    private var mSelection = 0

    private val mConnection = object : ServiceConnection {

        override fun onServiceConnected(p1: ComponentName, binder: IBinder) {
            mBinder = binder as DownloadManagerService.DMBinder
            mManager = mBinder!!.downloadManager
        }

        override fun onServiceDisconnected(p1: ComponentName) {
            mBinder = null
            mManager = null
        }

    }

    public override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        mToggle!!.syncState()
    }


    @TargetApi(21)
    override fun onCreate(savedInstanceState: Bundle?) {
        CrashHandler.init(this)
        CrashHandler.register()

        // Service
        val i = Intent()
        i.setClass(this, DownloadManagerService::class.java)
        startService(i)
        bindService(i, mConnection, Context.BIND_AUTO_CREATE)

        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayUseLogoEnabled(false)

        mPrefs = getSharedPreferences("threads", Context.MODE_PRIVATE)

        // Drawer
        mDrawer = Utility.findViewById<DrawerLayout>(this, R.id.drawer)
        mToggle = ActionBarDrawerToggle(this, mDrawer, mToolbar, 0, 0)
        mToggle!!.isDrawerIndicatorEnabled = true
        mDrawer!!.addDrawerListener(mToggle!!)

        if (Build.VERSION.SDK_INT >= 21) {
            findViewById<View>(R.id.nav).elevation = 20.0f
        } else {
            mDrawer!!.setDrawerShadow(R.drawable.drawer_shadow, Gravity.LEFT)
        }

        mList = Utility.findViewById<ListView>(this, R.id.nav_list)
        mAdapter = NavigationAdapter(this, R.array.drawer_items, R.array.drawer_icons)
        mList!!.adapter = mAdapter

        // FAB
        FloatingActionButton.Builder(this)
                .withButtonColor(ContextCompat.getColor(this, R.color.blue))
                .withButtonSize(80)
                .withDrawable(ContextCompat.getDrawable(this, R.drawable.ic_add_white_24dp))
                .withGravity(Gravity.RIGHT or Gravity.BOTTOM)
                .withPaddings(0, 0, 10, 10)
                .create()
                .setOnClickListener {
                    if (mFragment != null) {
                        showUrlDialog()
                    }
                }

        // Fragment
        window.decorView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                updateFragments()
                window.decorView.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
        mList!!.onItemClickListener = this

        // Intent
        if (intent.action == INTENT_DOWNLOAD) {
            mPendingUrl = intent.data!!.toString()
        }
    }

    override fun onResume() {
        super.onResume()

        if (mPendingUrl != null) {
            showUrlDialog()
            mPendingUrl = null
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        unbindService(mConnection)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        if (intent.action == INTENT_DOWNLOAD) {
            mPendingUrl = intent.data!!.toString()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.main
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mToggle!!.onConfigurationChanged(newConfig)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            permissionObserver.accept(grantResults[0] == PackageManager.PERMISSION_GRANTED)
        }
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        mDrawer!!.closeDrawer(Gravity.LEFT)
        if (position < 3) {
            if (position != mSelection) {
                mSelection = position
                updateFragments()
            }
        } else if (position == 4) {
            val i = Intent()
            i.action = Intent.ACTION_VIEW
            i.setClass(this, BrowserActivity::class.java)
            startActivity(i)
        } else if (position == 5) {
            val i = Intent()
            i.action = Intent.ACTION_VIEW
            i.setClass(this, SettingsActivity::class.java)
            startActivity(i)
        }
    }

    private fun updateFragments() {
        when (mSelection) {
            0 -> mFragment = AllMissionsFragment()
            1 -> mFragment = DownloadingMissionsFragment()
            2 -> mFragment = DownloadedMissionsFragment()
        }
        fragmentManager.beginTransaction()
                .replace(R.id.frame, mFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()

        for (i in 0..2) {
            val v = mList!!.getChildAt(i)

            val icon = Utility.findViewById<ImageView>(v, R.id.drawer_icon)
            val text = Utility.findViewById<TextView>(v, R.id.drawer_text)

            if (i == mSelection) {
                v.setBackgroundResource(R.color.light_gray)
                icon.setColorFilter(ContextCompat.getColor(this, R.color.blue), PorterDuff.Mode.SRC_IN)
                text.setTextColor(ContextCompat.getColor(this, R.color.blue))
            } else {
                v.setBackgroundResource(android.R.color.transparent)
                icon.colorFilter = null
                text.setTextColor(ContextCompat.getColor(this, R.color.gray))
            }
        }
    }

    private fun showUrlDialog() {
        // Create the view
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = inflater.inflate(R.layout.dialog_url, null)
        val text = Utility.findViewById<EditText>(v, R.id.url)
        val name = Utility.findViewById<EditText>(v, R.id.file_name)
        val tCount = Utility.findViewById<TextView>(v, R.id.threads_count)
        val threads = Utility.findViewById<SeekBar>(v, R.id.threads)
        val toolbar = Utility.findViewById<Toolbar>(v, R.id.toolbar)
        val fetch = Utility.findViewById<Button>(v, R.id.fetch_name)

        threads.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                tCount.text = (progress + 1).toString()
            }

            override fun onStartTrackingTouch(p1: SeekBar) {

            }

            override fun onStopTrackingTouch(p1: SeekBar) {

            }

        })

        val def = mPrefs!!.getInt("threads", 4)
        threads.progress = def - 1
        tCount.text = def.toString()

        text.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(p1: CharSequence, p2: Int, p3: Int, p4: Int) {

            }

            override fun onTextChanged(p1: CharSequence, p2: Int, p3: Int, p4: Int) {

                val url = text.text.toString().trim { it <= ' ' }

                if (url != "") {
                    val index = url.lastIndexOf("/")

                    if (index > 0) {
                        var end = url.lastIndexOf("?")

                        if (end < index) {
                            end = url.length
                        }

                        name.setText(url.substring(index + 1, end))
                    }
                }
            }

            override fun afterTextChanged(txt: Editable) {

            }
        })

        if (mPendingUrl != null) {
            text.setText(mPendingUrl)
        }

        toolbar.setTitle(R.string.add)
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.inflateMenu(R.menu.dialog_url)

        // Show the dialog
        val dialog = AlertDialog.Builder(this)
                .setCancelable(true)
                .setView(v)
                .create()

        dialog.show()

        fetch.setOnClickListener { NameFetcherTask().execute(text, name) }

        toolbar.setNavigationOnClickListener { dialog.dismiss() }

        toolbar.setOnMenuItemClickListener { item ->
            if (item.itemId == R.id.okay) {

                permissionObserver = Consumer { granted ->
                    if (granted) {
                        onOkClicked(text, name, threads, dialog)
                    } else {
                        //TODO: check rationality
                    }
                }
                disposable.add(getPermissionObservable()
                        .subscribe(permissionObserver))

                return@setOnMenuItemClickListener true
            } else {
                return@setOnMenuItemClickListener false
            }
        }

    }

    private fun getPermissionObservable(): Single<Boolean> {


        return Single.create<Boolean> {
            val granted = ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

            if (granted) it.onSuccess(true)
            else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
            }

        }
    }

    private fun onOkClicked(text: EditText, name: EditText, threads: SeekBar, dialog: AlertDialog) {
        val url = text.text.toString().trim { it <= ' ' }
        val fName = name.text.toString().trim { it <= ' ' }

        val f = File(mManager!!.location + "/" + fName)

        if (f.exists()) {
            Toast.makeText(this@MainActivity, R.string.msg_exists, Toast.LENGTH_SHORT).show()
        } else if (!checkURL(url)) {
            Toast.makeText(this@MainActivity, R.string.msg_url_malform, Toast.LENGTH_SHORT).show()
        } else {

            while (mBinder == null);

            val res = mManager!!.startMission(url, fName, threads.progress + 1)
            mBinder!!.onMissionAdded(mManager!!.getMission(res))
            mFragment!!.notifyChange()

            mPrefs!!.edit().putInt("threads", threads.progress + 1).apply()
            dialog.dismiss()
        }
    }

    private fun checkURL(url: String): Boolean {
        return try {
            val u = URL(url)
            u.openConnection()
            true
        } catch (e: MalformedURLException) {
            false
        } catch (e: IOException) {
            false
        }

    }

    private class NameFetcherTask : AsyncTask<View, Void, Array<Any>>() {

        override fun doInBackground(params: Array<View>): Array<Any>? {
            try {
                val url = URL((params[0] as EditText).text.toString())
                val conn = url.openConnection() as HttpURLConnection
                val header = conn.getHeaderField("Content-Disposition")

                if (header != null && header.contains("=")) {
                    return arrayOf(params[1], header.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1].replace("\"", ""))
                }
            } catch (e: Exception) {

            }

            return null
        }

        override fun onPostExecute(result: Array<Any>?) {
            super.onPostExecute(result)

            if (result != null) {
                (result[0] as EditText).setText(result[1].toString())
            }
        }
    }

    companion object {

        const val INTENT_DOWNLOAD = "com.parsdroid.nightdownloader.intent.DOWNLOAD"
        private const val PERMISSION_REQUEST_CODE = 61
    }

}
