package com.parsdroid.nightdownloader.ui.fragment;

import com.parsdroid.nightdownloader.get.DownloadManager;
import com.parsdroid.nightdownloader.get.FilteredDownloadManagerWrapper;
import com.parsdroid.nightdownloader.service.DownloadManagerService;

public class DownloadedMissionsFragment extends MissionsFragment {
    @Override
    protected DownloadManager setupDownloadManager(DownloadManagerService.DMBinder binder) {
        return new FilteredDownloadManagerWrapper(binder.getDownloadManager(), true);
    }
}
