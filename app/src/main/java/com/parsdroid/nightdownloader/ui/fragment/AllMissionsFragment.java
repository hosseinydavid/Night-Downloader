package com.parsdroid.nightdownloader.ui.fragment;

import com.parsdroid.nightdownloader.get.DownloadManager;
import com.parsdroid.nightdownloader.service.DownloadManagerService;

public class AllMissionsFragment extends MissionsFragment {

    @Override
    protected DownloadManager setupDownloadManager(DownloadManagerService.DMBinder binder) {
        return binder.getDownloadManager();
    }
}
